// console.log("Hello World!");

// [SECTION] - Javascript Synchronous vs Asynchronous
// JS --> Synchronous - it can execute one statement or  one line of code at a time.

console.log("Hello World!");
// conosle.log("Hello Again!");
console.log("Goodbye");

console.log("Hello World");
// for (let i = 0; i <= 1500; i++){
// 	console.log(i);
// }
console.log("Hello Again!");

// [SECTION] - Getting all post
// fetch() - fetch request
// The fetch api allows programmers to asynchronously for a resource (data).

/*
	Syntax -- //fetch()
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'))
*/

fetch("https://jsonplaceholder.typicode.com/posts")
// We use "json" method from the response object to convert data into JSON format
.then((response) => response.json())
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be use to achieve asynchronous
// Used in function to indicate which portions of the code should be awaited for
// Creates an asynchronous function 

async function fetchData(){
	
	// waits for the "fetch" metod to complete them stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");
	// result returned "response" -- expecting a promise
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
};

fetchData();

// [SECTION] Getting a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Creating A post

/*
	Syntax:
	fetch("URL", options)
	.then((response) => {})
	.then((response) => {})
*/

fetch("https://jsonplaceholder.typicode.com/posts", {
	method: "POST",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "New Post",
		body: "Hello WOrld",
		userID: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Updating a post 
// PUT Method is used to update a specific data

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		id: 1, 
		title: "Updated Post",
		body: "Hello Again!",
		userID: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Patch method 
// Update a specific post, specific property in a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Post"
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Deleting a post

fetch("https://jsonplaceholder.typicode.com/posts/1", {
	method: "DELETE"
});

// [SECTION] Filtering Post
/*
	SYNTAX
	Individual Parameter
	//'url?parameterName=value'
	Multiple Parameter
	//url?paramA=valueA&paramB=valueB

*/

fetch("https://jsonplaceholder.typicode.com/posts?userId=1")
.then((response) => response.json())
.then((json) => console.log(json));

// [SECTION] Retrieving comments on a specific post

fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
.then((response) => response.json())
.then((json) => console.log(json));